import random

class RandomStringGenerator:
    """
    Generates a line of completely random string.
    No meanigful words or formatting.
    """

    def __init__(self, options):
        self.options = options

    def generate(self, alphabet):
        character_count = random.randint(*self.options["wordLengthRange"])

        out = ""
        last_char = " " # space to not begin with space

        while len(out) < character_count:

            # try to double last character - DISABLED
            """
            if self.options["boostDoubleCharacters"] \
                    and random.randint(0, 100) <= self.options["doublingProbability"] \
                    and last_char != " ":
                next_char = last_char
            """

            # try to generate space
            if self.options["boostSpaces"] \
                    and last_char != " " \
                    and random.randint(0, 100) <= self.options["spaceProbability"]:
                next_char = " "

            # generate just an ordinary character
            else:
                next_char = random.choice(alphabet.characters)

            # never allow two spaces next to each other
            if next_char == " " and last_char == " ":
                continue

            # never allow last character to be space
            if next_char == " " and len(out) == character_count - 1:
                continue

            out += next_char
            last_char = next_char

        return out.strip() # no start or end with spaces (shouldn't, but just to make sure)
