from text_generator.random_string_generator import RandomStringGenerator
from utilities.alphabet import Alphabet

if __name__ == "__main__":
    gen = RandomStringGenerator({
        "wordLengthRange": (1, 5),
        "boostSpaces": True,
        "spaceProbability": 30
    })

    for i in range(20):
        print(gen.generate(Alphabet.Latin))
