import matplotlib.pyplot as plt
import numpy as np
from image_generator.line_image_generator import LineImageGenerator
from image_generator.effector import Effector
from utilities.alphabet import Alphabet

def main():
    gen = LineImageGenerator({
        "fonts": "all",
        "imageHeight": 32,
        "applyTransforms": True
    }, {
        "wordLengthRange": (1, 5),
        "boostSpaces": True,
        "spaceProbability": 30
    })

    eff = Effector({
        "badPrinter": 0.5,
        "blur": 0.5
    })

    img = gen.generate_block(eff)
    #img = gen.generate_block()
    #img, _ = gen.generate("Lorem ipsum greaT")

    plt.imshow(np.dstack([img, img, img]))
    #plt.imshow(img)
    plt.show()

if __name__ == "__main__":
    main()
