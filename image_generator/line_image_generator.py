from text_generator.random_string_generator import RandomStringGenerator
from utilities.font_loader import FontLoader
from PIL import Image, ImageDraw, ImageFont
import numpy as np
import string
import os
import cv2
import random
import math

class LineImageGenerator:
    """Generates an image of a line of text."""

    def __init__(self, options, text_generator_options):
        # string generator
        self.string_generator = RandomStringGenerator(text_generator_options)

        self.options = options

        # output line height in pixels; line width get's calculated
        self.line_height = options["imageHeight"]

        # how much to scale the image (up and down) for the font to be smooth
        self.supersizing = 3

    def get_text_dimensions(self, text, font):
        img = Image.new("RGB", (10, 10), (255, 255, 255))
        draw = ImageDraw.Draw(img)
        return draw.textsize(text, font=font)

    ##############
    # Generation #
    ##############

    def generate_source_image(self, text, font):
        """Generates a superscaled image, where the text touches
        the border and no distortion is applied. The height may wary due to
        the text being rendered. The text will be cropped precisely."""
        pil_font = font.get_pil_font(self.supersizing * self.line_height)

        text_width, text_height = self.get_text_dimensions(text, pil_font)

        img = Image.new(
            "RGB",
            (text_width, text_height),
            (255, 255, 255)
        )
        draw = ImageDraw.Draw(img)

        # render
        draw.text(
            (0, 0),
            text,
            (0, 0, 0),
            font=pil_font
        )

        # return grayscale numpy array
        return np.array(img)[:,:,0]

    def generate_transformation(self, source_shape):
        """Generates a random affine transformation for an image."""
        # skew the text (and scale on X axis)
        max_skew_angle = 15 # deg
        max_scale = 1.2
        skew_angle = random.randint(-max_skew_angle, max_skew_angle)
        scale = random.uniform(1 / max_scale, max_scale)
        skew_shift = 100 * math.tan(skew_angle / 180 * math.pi)
        pts1 = np.float32([[0, 0], [100, 0], [0, 100]])
        pts2 = np.float32([[0, 0], [100 * scale, 0], [skew_shift, 100]])
        skew_matrix = cv2.getAffineTransform(pts1,pts2)

        # rotate the text (such that the rise at the end is at most half the height)
        max_angle = 0
        if source_shape[0] / source_shape[1] < 1:
            max_angle = int(math.asin(source_shape[0] / source_shape[1]) / math.pi * 90)
        if max_angle > 15:
            max_angle = 15

        angle = random.randint(-max_angle, max_angle)
        rotation_matrix = cv2.getRotationMatrix2D((0, 0), angle, 1)

        # compose transformations
        matrix = np.matrix(skew_matrix[0:2,0:2]) * np.matrix(rotation_matrix[0:2,0:2])

        matrix = np.zeros(shape=(2, 3), dtype=np.float32)
        matrix[0:2, 0:2] = rotation_matrix[0:2,0:2].dot(skew_matrix[0:2,0:2])

        return matrix

    def generate_padding(self):
        """Generates a random padding (supersized)"""
        horizontal_max = self.line_height * self.supersizing * 0.5
        vertical_max = 4 * self.supersizing
        left = random.randint(0, horizontal_max)
        right = random.randint(0, horizontal_max)
        top = random.randint(0, vertical_max)
        bottom = random.randint(0, vertical_max)
        return (top, right, bottom, left)

    def generate_transformed_image(self, text, font):
        """Generates a transformed, supersized image."""
        source = self.generate_source_image(text, font)

        # setup the transformation matrix
        matrix = self.generate_transformation(source.shape)

        # setup padding (top, right, bottom, left)
        padding = self.generate_padding()

        # transform image corners to get the new dimensions (rect)
        corners = np.array([
            [[0, 0]],
            [[source.shape[1], 0]],
            [[source.shape[1], source.shape[0]]],
            [[0, source.shape[0]]]
        ], dtype=np.int32)
        transformed_corners = cv2.transform(corners, matrix)
        rect = cv2.boundingRect(transformed_corners) # (left, top, width, height)

        # move the image so that it fits the destination image (include padding)
        matrix[0, 2] += -rect[0] + padding[3]
        matrix[1, 2] += -rect[1] + padding[0]

        # calculate dest image size
        dest_size = (
            rect[2] + padding[3] + padding[1],
            rect[3] + padding[0] + padding[2]
        )

        # do the final image transformation
        img = cv2.warpAffine(source, matrix, dest_size, borderValue=255)

        return img

    def generate_image(self, text, font):
        """Generates a random image with proper height"""
        if self.options["applyTransforms"]:
            img = self.generate_transformed_image(text, font)
        else:
            img = self.generate_source_image(text, font)

        width = int(self.line_height * (img.shape[1] / img.shape[0]))
        return cv2.resize(img, dsize=(width, self.line_height))

    def generate(self, text=None):
        """Generate a data sample"""
        if self.options["fonts"] == "all":
            font = FontLoader.get_random_font()
        else:
            font = FontLoader.get_font(random.choice(self.options["fonts"]))
        
        if text is None:
            alphabet = font.alphabet
            if self.options["alphabet"] is not None: # explicit restricted alphabet
                alphabet = self.options["alphabet"]
            text = self.string_generator.generate(alphabet)
            
        return self.generate_image(text, font), text

    def generate_block(self, effector=None):
        """Generates a block of samples for debugging"""
        height = 10
        height_px = height * self.line_height
        width_px = height_px * 2
        out = np.zeros(shape=(height_px, width_px), dtype=np.uint8)
        for i in range(height):
            y = i * self.line_height
            x = 0
            while True:
                img, _ = self.generate()

                if effector is not None:
                    img = effector.apply(img)

                if x + img.shape[1] > width_px:
                    break
                out[y:y+img.shape[0],x:x+img.shape[1]] = img
                x += img.shape[1] + 5
        return out
