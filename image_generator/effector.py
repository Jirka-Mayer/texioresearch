import cv2
import random
import numpy as np
from image_generator.perlin import generate_perlin

class Effector:
    def __init__(self, options):
        self.options = options

    def apply(self, img):
        if random.uniform(0, 1) <= self.options["badPrinter"]:
            img = self.bad_printer(img)

        if random.uniform(0, 1) <= self.options["blur"]:
            img = self.blur(img)

        return img

    def bad_printer(self, img):
        noise = generate_perlin(img.shape[1], img.shape[0], scale=3)

        mask = np.where(noise <= 64, 255 * 0.95, 0)
        mask = self.blur(mask)

        img = img.astype(np.int32) + mask
        return np.clip(img, 0, 255).astype(np.uint8)

    def blur(self, img):
        max_blur = 3 # (5x5)
        x = random.randint(1, max_blur) * 2 - 1
        y = random.randint(1, max_blur) * 2 - 1
        return cv2.GaussianBlur(img, (x, y), 0)
