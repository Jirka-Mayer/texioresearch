import numpy as np
import tensorflow as tf
import os
import cv2
import shutil
from utilities.alphabet import Alphabet
from utilities.sparse_tensor_from_sequences import sparse_tensor_from_sequences

class Network:
    IMAGE_HEIGHT = 32 # fixed by the CNN block architecture
    NETWORK_SCOPE = "network"

    def __init__(self, continual_saving=False, name=None, threads=1):
        
        # does the network save itself on improvement during dev evaluation?
        self.continual_saving = continual_saving

        # name of the model (for continual saving)
        self.name = name

        # alphabet that is used by the decoder
        self.alphabet = Alphabet.LatinExtended

        self.has_summaries = False

        # Create an empty graph and a session
        self.graph = tf.Graph()
        self.session = tf.Session(
            graph=self.graph,
            config=tf.ConfigProto(
                inter_op_parallelism_threads=threads,
                intra_op_parallelism_threads=threads
            )
        )

    def construct(self, logdir=None):
        with self.session.graph.as_default():

            # Inputs
            self.images = tf.placeholder(tf.float32, [None, Network.IMAGE_HEIGHT, None], name="images")
            self.image_widths = tf.placeholder(tf.int32, [None], name="widths")
            self.texts = tf.sparse_placeholder(tf.int32, name="texts")

            self.is_training = tf.placeholder(tf.bool, [], name="is_training")
            self.learning_rate = tf.placeholder(tf.float32, [], name="learning_rate")

            # trainable part
            with tf.variable_scope(Network.NETWORK_SCOPE):
                # CNN
                cnnOut4d, widths = self.setupCNN(
                    self.images, self.image_widths
                )

                # RNN
                logits = self.setupRNN(cnnOut4d)

            # CTC
            losses = self.setupCTC(
                logits,
                self.texts,
                widths
            )

            # Training
            self.training = self.setupTraining(self.learning_rate, self.loss)

            # Summaries
            if logdir is not None:
                self.setupSummaries(losses, logdir)
                self.has_summaries = True
            
            self.reset_metrics = tf.variables_initializer(tf.get_collection(tf.GraphKeys.METRIC_VARIABLES))

            # Initialize variables
            self.session.run(tf.global_variables_initializer())

            # Saver
            self.saver = tf.train.Saver(
                var_list=tf.get_collection(
                    tf.GraphKeys.GLOBAL_VARIABLES,
                    #scope=Network.NETWORK_SCOPE # nope, save it with global_step
                )
            )

    def setupCNN(self, cnnIn3d, widths):
        """Creates the CNN layers and returns output of these layers"""
        cnnIn4d = tf.expand_dims(input=cnnIn3d, axis=3)

        # list of parameters for the layers
        kernelVals = [5, 5, 3, 3, 3]
        featureVals = [1, 32, 64, 128, 128, 256]
        strideVals = poolVals = [(2,2), (2,2), (2,1), (2,1), (2,1)]
        numLayers = len(strideVals)

        # create layers
        pool = cnnIn4d # input to first CNN layer
        for i in range(numLayers):
            kernel = tf.Variable(
                tf.truncated_normal(
                    [kernelVals[i], kernelVals[i], featureVals[i], featureVals[i + 1]],
                    stddev=0.1
                )
            )
            conv = tf.nn.conv2d(pool, kernel, padding='SAME',  strides=(1,1,1,1))
            # X <- possible batch normalization here
            relu = tf.nn.relu(conv)
            pool = tf.nn.max_pool(
                relu,
                (1, poolVals[i][0], poolVals[i][1], 1),
                (1, strideVals[i][0], strideVals[i][1], 1),
                'VALID'
            )
            
            # update widths of the images
            # (for RNN layers and CTC to know how wide is meaningful data)
            if poolVals[i][1] == 2:
                widths = tf.floor_div(widths, tf.fill(tf.shape(widths), 2))

        return pool, widths

    def setupRNN(self, rnnIn4d):
        """Creates the RNN layers and returns output of these layers"""
        rnnIn3d = tf.squeeze(rnnIn4d, axis=[1])

        # basic cells which are used to build RNN
        numHidden = 256
        cells = [tf.contrib.rnn.LSTMCell(num_units=numHidden, state_is_tuple=True) for _ in range(2)] # 2 layers

        # stack basic cells
        stacked = tf.contrib.rnn.MultiRNNCell(cells, state_is_tuple=True)

        # bidirectional RNN
        # BxTxF -> BxTx2H
        ((fw, bw), _) = tf.nn.bidirectional_dynamic_rnn(cell_fw=stacked, cell_bw=stacked, inputs=rnnIn3d, dtype=rnnIn3d.dtype)
        
        # BxTxH + BxTxH -> BxTx2H -> BxTx1x2H
        concat = tf.expand_dims(tf.concat([fw, bw], 2), 2)
        
        # project output to chars (including blank): BxTx1x2H -> BxTx1xC -> BxTxC
        kernel = tf.Variable(tf.truncated_normal([1, 1, numHidden * 2, self.alphabet.size + 1], stddev=0.1))
        return tf.squeeze(tf.nn.atrous_conv2d(value=concat, filters=kernel, rate=1, padding='SAME'), axis=[2])

    def setupCTC(self, logits, label_texts, image_widths):
        """Creates the CTC loss and returns individual losses and their mean"""

        # time major
        logits = tf.transpose(logits, [1, 0, 2])

        # loss
        losses = tf.nn.ctc_loss(
            label_texts, # labels
            logits, # logits (network output)
            image_widths # vector of logit lengths
        )
        self.loss = tf.reduce_mean(losses)

        # beam predictions
        top_beam_predictions, _ = tf.nn.ctc_beam_search_decoder(
            logits,
            image_widths,
            merge_repeated=False
        )
        self.predictions = top_beam_predictions[0] # It's a single-element list

        # output nodes for android
        tf.identity(self.predictions.indices, name="android_predictions_indices")
        tf.identity(self.predictions.values, name="android_predictions_values")
        tf.identity(self.predictions.dense_shape, name="android_predictions_dense_shape")

        # greedy predictions
        top_greedy_predictions, _ = tf.nn.ctc_greedy_decoder(
            logits,
            image_widths
        )
        self.greedy_predictions = top_greedy_predictions[0] # It's a single-element list

        # output nodes for android
        tf.identity(self.greedy_predictions.indices, name="android_greedy_predictions_indices")
        tf.identity(self.greedy_predictions.values, name="android_greedy_predictions_values")
        tf.identity(self.greedy_predictions.dense_shape, name="android_greedy_predictions_dense_shape")

        # edit distance
        self.edit_distance = tf.reduce_mean(tf.edit_distance(self.predictions, tf.cast(label_texts, tf.int64)))
        self.greedy_edit_distance = tf.reduce_mean(tf.edit_distance(self.greedy_predictions, tf.cast(label_texts, tf.int64)))

        return losses

    def setupTraining(self, learning_rate, loss):
        """Creates an optimizer"""
        self.global_step = tf.train.create_global_step()
        #self.training = tf.train.AdamOptimizer().minimize(self.loss, global_step=global_step, name="training")
        #self.training = tf.train.MomentumOptimizer(1e-2, 0.9
        #).minimize(self.loss, global_step=global_step, name="training")
        
        return tf.train.RMSPropOptimizer(learning_rate).minimize(
            loss,
            global_step=self.global_step,
            name="training"
        )

    def setupSummaries(self, losses, logdir):
        """Creates summaries"""
        self.current_edit_distance, self.update_edit_distance = tf.metrics.mean(self.edit_distance)
        self.current_greedy_edit_distance, self.update_greedy_edit_distance = tf.metrics.mean(self.greedy_edit_distance)
        self.current_loss, self.update_loss = tf.metrics.mean(losses)
        
        summary_writer = tf.contrib.summary.create_file_writer(logdir, flush_millis=10 * 1000)

        self.summaries = {}

        with summary_writer.as_default(), tf.contrib.summary.record_summaries_every_n_global_steps(10):
            self.summaries["train"] = [
                tf.contrib.summary.scalar("train/loss", self.update_loss),
                tf.contrib.summary.scalar("train/edit_distance", self.update_greedy_edit_distance)
            ]

        with summary_writer.as_default(), tf.contrib.summary.always_record_summaries():
            for dataset in ["dev", "test"]:
                self.summaries[dataset] = [
                    tf.contrib.summary.scalar(dataset + "/loss", self.current_loss),
                    tf.contrib.summary.scalar(dataset + "/edit_distance", self.current_edit_distance)
                ]

        with summary_writer.as_default():
            tf.contrib.summary.initialize(session=self.session, graph=self.session.graph)

    #####################
    # Model persistence #
    #####################

    def load(self, model_name):
        """Loads the model of a given name"""
        self.saver.restore(
            self.session,
            self.get_model_path(model_name)
        )

    def exists(self, model_name):
        """Returns true if a given model exists"""
        return os.path.isdir(self.get_model_directory(model_name))

    def load_if_exists(self, model_name):
        """Loads a model, but only if it exists"""
        if self.exists(model_name):
            self.load(model_name)

    def save(self, model_name, edit_distance=None):
        """Saves the model and also saves edit distance if provided"""
        dirname = self.get_model_directory(model_name)
        if not os.path.exists(dirname):
            os.mkdir(dirname)

        self.saver.save(
            self.session,
            self.get_model_path(model_name)
        )

        if edit_distance is not None:
            self.save_edit_distance(model_name, edit_distance)

    def get_saved_edit_distance(self, model_name):
        """Returns edit distance of the saved model"""
        if not self.exists(model_name):
            return float("inf")
            
        with open(self.get_model_path(model_name) + ".edit_distance", "r") as file:
            ed = float(file.read())
        return ed

    def save_edit_distance(self, model_name, edit_distance):
        """Saves the edit distance"""
        dirname = self.get_model_directory(model_name)
        if not os.path.exists(dirname):
            os.mkdir(dirname)

        with open(self.get_model_path(model_name) + ".edit_distance", "w") as file:
            file.write(str(edit_distance))

    def save_if_better(self, model_name, edit_distance):
        """Saves the model only if it has smaller edit distance, than the saved"""
        if self.get_saved_edit_distance(model_name) > edit_distance:
            print("Saving...")
            self.save(self.name, edit_distance)

    def delete_if_exists(self, model_name):
        if self.exists(model_name):
            dirname = self.get_model_directory(model_name)
            shutil.rmtree(dirname, ignore_errors=True)

    def get_model_directory(self, model_name):
        """Returns directory path of a given model name"""
        return os.path.dirname(os.path.realpath(__file__)) + "/model/" + model_name

    def get_model_path(self, model_name):
        """Returns path for tensorflow saver to save the model to"""
        return self.get_model_directory(model_name) + "/model"

    def freeze(self, model_name):
        output_graph_def = tf.graph_util.convert_variables_to_constants(
            self.session,
            self.graph.as_graph_def(),
            [
                "android_predictions_indices",
                "android_predictions_values",
                "android_predictions_dense_shape",

                "android_greedy_predictions_indices",
                "android_greedy_predictions_values",
                "android_greedy_predictions_dense_shape"
            ]
        )

        filename = self.get_model_path(model_name) + ".pb"
        with tf.gfile.GFile(filename, "wb") as f:
            f.write(output_graph_def.SerializeToString())

        filename = self.get_model_path(model_name) + "_alphabet.txt"
        with tf.gfile.GFile(filename, "w") as f:
            f.write(self.alphabet.characters)

        print("Model has been frozen.")

    #########################
    # Training & prediction #
    #########################

    def train(self, train_dataset, dev_dataset, epochs, batch_size):
        for epoch in range(epochs):
            self.train_epoch(train_dataset, dev_dataset, epoch + 1, epochs, batch_size)

    def train_epoch(self, train_dataset, dev_dataset, epoch, epochs, batch_size):
        batches = train_dataset.get_batches(batch_size)
        batch = 1
        train_dataset.prepare_epoch()
        while train_dataset.has_batch():

            images, texts, widths = train_dataset.next_batch(batch_size)

            rate = self.calculate_learning_rate(self.get_global_step())

            # vars to evaluate
            evaluate = [self.loss, self.greedy_edit_distance, self.training]
            if self.has_summaries:
                evaluate = [self.loss, self.greedy_edit_distance, self.training, self.summaries["train"]]

            # train
            self.session.run(self.reset_metrics)
            evaluated = self.session.run(evaluate, {
                self.images: images,
                self.texts: sparse_tensor_from_sequences(texts),
                self.image_widths: widths,
                self.is_training: True,
                self.learning_rate: rate
            })

            loss = evaluated[0]
            greedy_edit_distance = evaluated[1]

            print("Epoch: %d/%s Batch: %d/%d Loss: %f ED: %f" % (
                epoch, str(epochs), batch, batches, loss, greedy_edit_distance
            ))

            batch += 1

        # and evaluate the performance after the epoch
        return self.evaluate(dev_dataset, batch_size)

    def evaluate(self, dataset, batch_size, dataset_name="dev"):
        """Calculates dev summaries and prints predictions for debug"""
        batches = dataset.get_batches(batch_size)
        batch = 1
        
        self.session.run(self.reset_metrics)
        dataset.prepare_epoch()

        right_items = 0
        all_items = 0
        wrong_examples = []

        while dataset.has_batch():
            images, texts, widths = dataset.next_batch(batch_size)
            predictions, _, _ = self.session.run([
                self.predictions,
                self.update_edit_distance,
                self.update_loss
            ], {
                self.images: images,
                self.texts: sparse_tensor_from_sequences(texts),
                self.image_widths: widths,
                self.is_training: False
            })

            all_items += batch_size
            offset = 0
            for i in range(len(texts)):
                indices = predictions.indices[predictions.indices[:,0] == i, 1]
                l = 0 if len(indices) == 0 else indices.max() + 1
                label = self.alphabet.decode(texts[i])
                pred = self.alphabet.decode(predictions.values[offset:offset+l])
                ok = "[ok]" if label == pred else "[err]"
                if label == pred: right_items += 1
                print(ok, label, "->", pred)
                offset += l
                if label != pred: wrong_examples.append((label, pred))
            
            print("Batch: %d / %d" % (batch, batches))

            batch += 1

        word_accuracy = (right_items / all_items) * 100
        edit_distance, loss = self.session.run([
            self.current_edit_distance,
            self.current_loss
        ])
        print("Edit distance: %f Word accuracy: %f%% Loss: %f" % (edit_distance, word_accuracy, loss))

        if word_accuracy >= 10: # do not show completely terrible results
            print("Some wrong examples:")
            for i in range(min(10, len(wrong_examples))):
                print(wrong_examples[i][0], "->", wrong_examples[i][1])

        # save validation loss and edit distance to summaries
        if self.has_summaries:
            self.session.run(self.summaries[dataset_name])

        # perform continual saving
        if self.continual_saving:
            self.save_if_better(self.name, edit_distance)

        # return loss and edit distance
        return loss, edit_distance

    def predict(self, img):
        """Predicts text in a single image"""
        if len(img.shape) == 3:
            img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

        width = img.shape[1]
        assert img.shape[0] == Network.IMAGE_HEIGHT

        predictions = self.session.run(self.predictions, {
            self.images: [img / 255.0],
            self.image_widths: [width],
            self.is_training: False
        })

        return self.alphabet.decode(predictions.values)

    def get_global_step(self):
        """Returns value of the global step"""
        return self.session.run(self.global_step)

    def calculate_learning_rate(self, batchesTrained):
        if batchesTrained > 10000:
            return 0.0001
        elif batchesTrained > 10:
            return 0.001
        else:
            return 0.01
