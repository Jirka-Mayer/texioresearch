List of experiments
===================

## [A]

*Network*
Default

*Training*
Batch size: 50
Epochs: 40
Training dataset size: 25000 (500 batches)
Validation dataset size: 500 (10 batches)

*Learning rate*  (default for HTR)
0 - 10     => 0.01
10 - 10000 => 0.001
10000 +    => 0.0001

*Data*
- no padding
- no transformation
- single font (merchant copy regular)
- ascii letters lowercase only (no spaces, 1-5 characters)

> Results:
> - problems with repeating letters -> missing in the dataset
> - converges after 1000 batches "most of the time"
> - learns to first output empty string
> - suprisingly generalized to padded images (data for experiment B)


## [B]

*Network*
Default

*Training*
Batch size: 50
Epochs: 40
Training dataset size: 25000 (500 batches)
Validation dataset size: 500 (10 batches)

*Learning rate*  (default for HTR)
0 - 10     => 0.01
10 - 10000 => 0.001
10000 +    => 0.0001

*Data*
- `with padding`
- no transformation
- single font (merchant copy regular)
- ascii letters lowercase only (no spaces, 1-5 characters)

> Results:
> - does not converge (same way the model A sometimes refuses to converge)

## [C]

Same as the experiment B, except the model is initialized to a converged model of A.

> Results:
> - model converges very quickly (few batches)

# [D]

*Data*
- `with padding`
- `with transformations (skew, rotate)`

Initialized as the result of C.

> Results:
> - model did converge, accuracy is almost 100%, only errors are double-letter errors

# [E]

Tests straight phase convergence on `Alphabet.LatinNoSpace`. Records the epoch of convergence.

> Results:
> Convergence after epoch: [1, X, X, X, X, 1, 1, 1, X, X, X, X, X, X, X, 1, X, 1]
> Double-characters are still a problem.

> Idea: Consider gradient clipping to prevent loss spikes from happening?

# [F]

Training a network on full latin dataset (not extended-latin due to lack of font support), including spaces. Spaces are boosted (30%) and double characters are also boosted (20%). Double spaces are not allowed.

> Convergences: [X, X, X, X, 1]
> Does work, learns faster, makes mistakes between 0 and O and so on. Expectable. However double characters are still a problem. Check CTC loss settings that it doesn't do this by default.

# [G]

Count successful convergences with only digits (simple alphabet). With spaces and doubleboost however.

> Convergences: [1, X, 1, 1, 1, X, 1, 1, 1, 1] # (note: half the epoch would be enough)

> Idea: Use greedy decoder to speedup training. See how much it speeds up.

> Sub-experiment: set (merge_repeated (decoder) to false)
> OMG it solves double-characters! (Dig deeper now.)
> ...
> Found the problem:
> https://github.com/tensorflow/tensorflow/issues/9550
>
> => set merge_repeated=False on BEAM
> => set merge_repeated=True on greedy

> Sub-experiment: Disabling doubling boost does not cripple performace -> remove it.

> Idea: Scale text width

# [H]

Simple postprocessing.

> Eh... 50% edit distance, but simmilar performance on real data. Too much change at once - useless experiment.

# [I]

Multifont, no postprocessing.

Interesting fonts:
http://www.receiptfont.com/

> Much better performance on the real receipt. The single font was definitely a problem.
> 90,6% word accuracy on validation dataset.

# [J]

Fixed effector apliaction on the dataset - now testing postprocessing properly.
Validation is performed on generated data.

> Validation performace much better (80-90% word acc.) due to the effector fix.

> Discovered a bug. I accidentaly removed simplified alphabet from bootstrap phase.

# [K]

Last phase is trained and tested on some real data. (validated on real data only)
Fixed alphabet problem - bootstrap phase works again.
Added `[]` to alphabet.

# [L]

Removed intermediate phases - going straigth from bootstraping to postprocessing phase.

> It has difficulties handlind the shock. Edit distance stays high
on postprocessing phase start.

Let's put back one epoch of straight phase.

> Well let's keep all phases, but reduce their epoch count to prevent overfitting on each.

Reduced epoch counts to 1 and dataset sizes to 1500 for intermediate phases.

Also loading of the new dataset format has been added.

# [M]

Larger dataset, 8 epochs of postprocessing phase instead of 5.

# [N]

Dataset size upped to 2200 real items. 12 postprocessing epochs.
