import random
import numpy as np
from image_generator.line_image_generator import LineImageGenerator
from utilities.alphabet import Alphabet
from image_generator.effector import Effector

class Dataset:
    def __init__(self, size, effector_options, image_generator_options, text_generator_options):
        # dataset size
        self.size = size

        # alphabet used for encoding (not data generation!)
        self.alphabet = Alphabet.LatinExtended

        # line height
        self.image_height = image_generator_options["imageHeight"]

        self.images = None
        self.texts = None

        self.generator = LineImageGenerator(image_generator_options, text_generator_options)

        self.effector_options = effector_options
        self.effector = Effector(effector_options)

        self.permutation = None

        self.generate()

    def check_dataset_visually(self):
        """Method for debugging"""
        import matplotlib.pyplot as plt

        for i in range(10):
            index = random.randint(0, self.size - 1)
            print("Data item text: %s" % self.alphabet.decode(self.texts[index]))
            plt.imshow(np.dstack([self.images[index], self.images[index], self.images[index]]))
            plt.show()

    ##############
    # Generation #
    ##############

    def generate(self):
        self.images = []
        self.texts = []
        for i in range(self.size):
            img, txt = self.generate_sequence()
            self.images.append(img)
            self.texts.append(txt)
        self.images = self.images
        self.texts = self.texts

    def generate_sequence(self):
        image, text = self.generator.generate()

        if self.effector_options is not None:
            image = self.effector.apply(image)

        return self.normalize_image(image), self.alphabet.encode(text)

    def normalize_image(self, image):
        """Normalizes image values to 0.0 - 1.0"""
        return image / 255.0

    ##################
    # Data interface #
    ##################

    def prepare_epoch(self):
        self.permutation = np.random.permutation(self.size)

    def has_batch(self):
        if self.permutation is None:
            return False
        elif len(self.permutation) == 0:
            return False
        return True

    def next_batch(self, batch_size=1):
        # take batch of indices
        take = min(batch_size, len(self.permutation))
        indices = self.permutation[0:take]
        self.permutation = self.permutation[take:]

        # resolve indices to data
        # and convert text to id sequence
        picked_images = []
        picked_texts = []
        for i in indices:
            picked_images.append(self.images[i])
            picked_texts.append(self.texts[i])

        # get maximum image width
        max_image_width = 0
        for i in picked_images:
            if i.shape[1] > max_image_width:
                max_image_width = i.shape[1]

        # create output image tensor and fill it
        image_tensor = np.empty(
            shape=(take, self.image_height, max_image_width),
            dtype=np.float32
        )
        image_widths = np.empty(
            shape=(take,),
            dtype=np.int32
        )

        for i in range(take):
            w = picked_images[i].shape[1]
            image_tensor[i, :, 0:w] = picked_images[i]
            image_tensor[i, :, w:] = 1.0 # pad with white
            image_widths[i] = w

        return image_tensor, picked_texts, image_widths

    def all(self):
        self.prepare_epoch()
        return self.next_batch(len(self.permutation))

    def get_batches(self, batch_size):
        return len(self.images) // batch_size
