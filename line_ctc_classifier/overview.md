# Line CTC classifier

A neural network able to recognize a single line of text. The main structure is following:

    images -> [CNN layers] -> [RNN layers] -> [CTC loss] -> predictions

Input images are grayscale, normalized to the range `[0.0, 1.0]`. Their height is `32px` exactly, to make sure that after going through the CNN block, the resulting height would be `1px` and thus could be fed into the RNN block without a need for flattening. Image width is variable, batch tensor width is the width of the widest image in the batch.

The model is based on this HTR model: https://github.com/githubharald/SimpleHTR


## Model API

First, you create an instance of the `Network` class and you need to construct the tensorflow computatioal model.

    model = Network(threads=4)
    model.construct(logdir=None|str)

Now we can load the model (optionally).

    model.load()

And do inference.

    my_text = model.predict(my_image) # opencv grayscale image [0, 255]

Or training.

    model.train(train_dataset, dev_dataset, epochs, batch_size)
    model.save()

Or evaluate performace on a dataset.

    model.evaluate(dataset, batch_size)


## Made decisions

- **Input images will be grayscale**, because receipts are usually not colored, the text is black on white paper. Color would only come from lighting (orange-yellow tint) and noise. It unnecessarily expands the input space, which puts more demands on the dataset and data generator. That would also cause the network have to have a larger capacity, thus be bigger and slower.


## Improvements to try out

- using GRU instead of LSTM
- using bidirectional RNN layers
- using BatchNorm in the CNN block for regularization and covariate shift reduction
- try to use greedy decoder during training for speedup (if significant)
- reduce amount of input binarization, keep more mid-tones


## Observations

- During training, first the model learns to output empty string and only after that begins to output meanigful strings.
- Learning process may get stuck, if the learning rate is too high. That's why the learning rate decay is very important for the model to converge.