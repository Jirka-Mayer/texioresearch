import os
import pickle
import random
import cv2

class RealDataset:
    def __init__(self):
        data = pickle.load(open("resources/word-dataset.pkl", "rb"))

        self.images = data["images"]
        self.labels = data["labels"]

        # remove words without a label
        # or with empty string as a label (TF has some problems, needs to be solved)
        i = 0
        while i < len(self.labels):
            if self.labels[i] == "": # only empty string can be present
                del self.images[i]
                del self.labels[i]
                i -= 1
            i += 1

        print("Loaded %d real words!" % len(self.images))

        # already scooped items
        self.scooped = 0

        # shuffle the words
        self.permutation = list(range(len(self.labels)))
        random.shuffle(self.permutation)

    @property
    def remaining(self):
        return max(len(self.permutation) - self.scooped, 0)

    def scoop_data_items(self, num, dataset):
        """Scoops a number of real data items and overwrites them in a dataset"""
        num = min(num, len(self.permutation) - self.scooped)
        if num <= 0:
            print("Ran out of real dataset items.")
            exit()

        if dataset.size < num:
            print("Target dataset is not large enough.")
            exit()

        for i in range(num):
            img = self.images[self.permutation[self.scooped + i]]
            text = self.labels[self.permutation[self.scooped + i]]

            dataset.images[i] = img
            dataset.texts[i] = dataset.alphabet.encode(text)

        self.scooped += num
