from line_ctc_classifier.network import Network
from line_ctc_classifier.trainer import Trainer
import argparse

def action_train():
    # training
    trainer = Trainer(
        model_name="N",
        threads=6
    )
    trainer.train_phases()

def action_freeze():
    network = Network()
    network.construct()
    network.load("N_postprocessing_phase")
    network.freeze("N_postprocessing_phase")

########
# Main #
########

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("action", type=str)
    args = parser.parse_args()

    if args.action == "train":
        action_train()
    elif args.action == "freeze":
        action_freeze()
    else:
        print("Unknown action.")
