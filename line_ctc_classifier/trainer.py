from utilities.alphabet import Alphabet
from line_ctc_classifier.network import Network
from line_ctc_classifier.dataset import Dataset
from line_ctc_classifier.real_dataset import RealDataset
import datetime
import os
import random

# when true, speedtest takes place
# its role is to test control code function before actual training
# effects:
# - reduce dataset sizes to 10
# - straight phase has 50% chance to converge regardless of the loss value every epoch
SPPED_TEST = False

class Trainer:
    def __init__(self, model_name, clear_saves=True, threads=1):
        self.model_name = model_name
        self.clear_saves = clear_saves # remove saved models before training
        self.threads = threads

        ###########################
        # Bootstrap phase options #
        ###########################

        self.bootstrap_phase_options = {
            "textGeneratorOptions": {
                "wordLengthRange": (1, 5),
                "boostSpaces": True,
                "spaceProbability": 30
            },

            "imageGeneratorOptions": {
                "alphabet": Alphabet.LatinNumbers,
                "fonts": ["merchant-copy.regular.ttf"],
                "imageHeight": Network.IMAGE_HEIGHT,
                "applyTransforms": False
            },

            "effectorOptions": None,

            "trainingDatasetSize": 10 if SPPED_TEST else 15000,
            "validationDatasetSize": 10 if SPPED_TEST else 500,
            "batchSize": 50,
            "successLossThreshold": 3.0 # when to consider convergence to occur
        }

        ##########################
        # Straight phase options #
        ##########################

        self.straight_phase_options = {
            "textGeneratorOptions": {
                "wordLengthRange": (1, 5),
                "boostSpaces": True,
                "spaceProbability": 30
            },

            "imageGeneratorOptions": {
                "alphabet": None, # no restriction, use font-specific
                "fonts": ["merchant-copy.regular.ttf"],
                "imageHeight": Network.IMAGE_HEIGHT,
                "applyTransforms": False
            },

            "effectorOptions": None,

            "trainingDatasetSize": 10 if SPPED_TEST else 15000,
            "validationDatasetSize": 10 if SPPED_TEST else 500,
            "batchSize": 50,
            "epochs": 1
        }

        ###########################
        # Distorted phase options #
        ###########################

        self.distorted_phase_options = {
            "textGeneratorOptions": self.straight_phase_options["textGeneratorOptions"],

            "imageGeneratorOptions": {
                "alphabet": None, # no restriction, use font-specific
                "fonts": ["merchant-copy.regular.ttf"],
                "imageHeight": Network.IMAGE_HEIGHT,
                "applyTransforms": True
            },

            "effectorOptions": None,

            "trainingDatasetSize": 10 if SPPED_TEST else 15000,
            "validationDatasetSize": 10 if SPPED_TEST else 500,
            "batchSize": 50,
            "epochs": 1
        }

        ###########################
        # Multifont phase options #
        ###########################

        self.multifont_phase_options = {
            "textGeneratorOptions": self.straight_phase_options["textGeneratorOptions"],

            "imageGeneratorOptions": {
                "alphabet": None, # no restriction, use font-specific
                "fonts": "all",
                "imageHeight": Network.IMAGE_HEIGHT,
                "applyTransforms": True
            },

            "effectorOptions": None,

            "trainingDatasetSize": 10 if SPPED_TEST else 15000,
            "validationDatasetSize": 10 if SPPED_TEST else 500,
            "batchSize": 50,
            "epochs": 1
        }

        ################################
        # Postprocessing phase options #
        ################################

        self.postprocessing_phase_options = {
            "textGeneratorOptions": self.straight_phase_options["textGeneratorOptions"],

            "imageGeneratorOptions": self.multifont_phase_options["imageGeneratorOptions"],

            "effectorOptions": {
                "badPrinter": 0.5,
                "blur": 0.5
            },

            "trainOnReal": True,
            "validateOnReal": True,

            "trainingDatasetSize": 10 if SPPED_TEST else 25000,
            "validationDatasetSize": 10 if SPPED_TEST else 500,
            "batchSize": 10,  # real data words are longer
            "epochs": 12
        }

    def get_phase_index(self, phase):
        phase_to_int = ["bootstrap", "straight", "distorted", "multifont", "postprocessing"]
        return phase_to_int.index(phase)

    def train_phases(self, start="bootstrap", end="postprocessing"):
        """Performs all currently accepted training steps, producing
        a fresh new network. You can optionally specify only a range
        of phases to be performed."""
        start = self.get_phase_index(start)
        end = self.get_phase_index(end)
        
        if start <= 0 and end >= 0:
            self.train_bootstrap_phase()

        if start <= 1 and end >= 1:
            self.train_straigth_phase()

        if start <= 2 and end >= 2:
            self.train_distorted_phase()

        if start <= 3 and end >= 3:
            self.train_multifont_phase()

        if start <= 4 and end >= 4:
            self.train_postprocessing_phase()

    def create_logdir(self, phase):
        if not os.path.exists("logs"):
            os.mkdir("logs")
        phase_index = self.get_phase_index(phase)
        return "logs/{}-{}".format(
            self.model_name + "_" + str(phase_index) + "_" + phase + "_phase",
            datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")
        )

    def train_bootstrap_phase(self):
        """Prepares the network for real training by bootstrapping it on numbers.
        It's meant to prevent the model from hitting local minima
        during first epochs of training."""
        print("")
        print("=========================")
        print("STARTING BOOTSTRAP PHASE!")
        print("=========================")
        print("")

        # phase params
        options = self.bootstrap_phase_options
        model = self.model_name + "_bootstrap_phase"

        print("Generating data...")
        train_dataset = Dataset(
            options["trainingDatasetSize"],
            options["effectorOptions"],
            options["imageGeneratorOptions"],
            options["textGeneratorOptions"]
        )
        dev_dataset = Dataset(
            options["validationDatasetSize"],
            options["effectorOptions"],
            options["imageGeneratorOptions"],
            options["textGeneratorOptions"]
        )

        print("Creating the model...")
        network = Network(
            continual_saving=True,
            name=model,
            threads=self.threads
        )
        network.construct(
            logdir=self.create_logdir("bootstrap")
        )

        # clear model
        if self.clear_saves:
            network.delete_if_exists(model)

        # train single epochs until convergence occurs
        print("Training...")
        converged = False
        epoch = 1
        while not converged:
            loss, _ = network.train_epoch(
                train_dataset,
                dev_dataset,
                epoch,
                "B",
                options["batchSize"]
            )

            if loss <= options["successLossThreshold"]:
                print("#############################")
                print("# Success threshold passed! #")
                print("#############################")
                converged = True

            # SPEED_TEST: 50% change to converge every epoch no matter what
            if SPPED_TEST and not converged:
                converged = random.choice([True, False])

            epoch += 1

        # no saving here, if the model improved, it would have been saved automatically

    def train_straigth_phase(self):
        """This phase sets-up the network. It trains on plain, unpadded,
        undistorted images with only one font - Merchant Copy."""
        print("")
        print("========================")
        print("STARTING STRAIGHT PHASE!")
        print("========================")
        print("")
        
        # phase params
        options = self.straight_phase_options
        model = self.model_name + "_straight_phase"

        print("Generating data...")
        train_dataset = Dataset(
            options["trainingDatasetSize"],
            options["effectorOptions"],
            options["imageGeneratorOptions"],
            options["textGeneratorOptions"]
        )
        dev_dataset = Dataset(
            options["validationDatasetSize"],
            options["effectorOptions"],
            options["imageGeneratorOptions"],
            options["textGeneratorOptions"]
        )

        print("Loading the model...")
        network = Network(
            continual_saving=True,
            name=model,
            threads=self.threads
        )
        network.construct(
            logdir=self.create_logdir("straight")
        )

        # load bootstrapped model
        network.load(self.model_name + "_bootstrap_phase")

        # clear model
        if self.clear_saves:
            network.delete_if_exists(model)

        print("Training...")
        network.train(
            train_dataset,
            dev_dataset,
            options["epochs"],
            options["batchSize"]
        )

        # again, no need for saving here

    def train_distorted_phase(self):
        """Trains with padded and distorted images of still only a single font."""
        print("")
        print("=========================")
        print("STARTING DISTORTED PHASE!")
        print("=========================")
        print("")
        
        # phase params
        options = self.distorted_phase_options
        model = self.model_name + "_distorted_phase"

        print("Generating data...")
        train_dataset = Dataset(
            options["trainingDatasetSize"],
            options["effectorOptions"],
            options["imageGeneratorOptions"],
            options["textGeneratorOptions"]
        )
        dev_dataset = Dataset(
            options["validationDatasetSize"],
            options["effectorOptions"],
            options["imageGeneratorOptions"],
            options["textGeneratorOptions"]
        )

        print("Loading the model...")
        network = Network(
            continual_saving=True,
            name=model,
            threads=self.threads
        )
        network.construct(
            logdir=self.create_logdir("distorted")
        )

        # load straight model
        network.load(self.model_name + "_straight_phase")

        # clear model
        if self.clear_saves:
            network.delete_if_exists(model)

        print("Training...")
        network.train(
            train_dataset,
            dev_dataset,
            options["epochs"],
            options["batchSize"]
        )

        # again, no need for saving here

    def train_multifont_phase(self):
        """Trains with all available fonts."""
        print("")
        print("=========================")
        print("STARTING MULTIFONT PHASE!")
        print("=========================")
        print("")
        
        # phase params
        options = self.multifont_phase_options
        model = self.model_name + "_multifont_phase"

        print("Generating data...")
        train_dataset = Dataset(
            options["trainingDatasetSize"],
            options["effectorOptions"],
            options["imageGeneratorOptions"],
            options["textGeneratorOptions"]
        )
        dev_dataset = Dataset(
            options["validationDatasetSize"],
            options["effectorOptions"],
            options["imageGeneratorOptions"],
            options["textGeneratorOptions"]
        )

        print("Loading the model...")
        network = Network(
            continual_saving=True,
            name=model,
            threads=self.threads
        )
        network.construct(
            logdir=self.create_logdir("multifont")
        )

        # load distorted model
        network.load(self.model_name + "_distorted_phase")

        # clear model
        if self.clear_saves:
            network.delete_if_exists(model)

        print("Training...")
        network.train(
            train_dataset,
            dev_dataset,
            options["epochs"],
            options["batchSize"]
        )

        # again, no need for saving here

    def train_postprocessing_phase(self):
        """Trains with post-processed images. (blur, noise, dilate, ...)"""
        print("")
        print("==============================")
        print("STARTING POSTPROCESSING PHASE!")
        print("==============================")
        print("")
        
        # phase params
        options = self.postprocessing_phase_options
        model = self.model_name + "_postprocessing_phase"

        print("Generating data...")
        train_dataset = Dataset(
            options["trainingDatasetSize"],
            options["effectorOptions"],
            options["imageGeneratorOptions"],
            options["textGeneratorOptions"]
        )
        dev_dataset = Dataset(
            options["validationDatasetSize"],
            options["effectorOptions"],
            options["imageGeneratorOptions"],
            options["textGeneratorOptions"]
        )

        real_dataset = RealDataset()

        if options["validateOnReal"]:
            real_dataset.scoop_data_items(
                options["validationDatasetSize"],
                dev_dataset
            )

        if options["trainOnReal"]:
            real_dataset.scoop_data_items(
                min(options["trainingDatasetSize"], real_dataset.remaining),
                train_dataset
            )

        print("Loading the model...")
        network = Network(
            continual_saving=True,
            name=model,
            threads=self.threads
        )
        network.construct(
            logdir=self.create_logdir("postprocessing")
        )

        # load multifont model
        network.load(self.model_name + "_multifont_phase")

        # clear model
        if self.clear_saves:
            network.delete_if_exists(model)

        print("Training...")
        network.train(
            train_dataset,
            dev_dataset,
            options["epochs"],
            options["batchSize"]
        )

        # again, no need for saving here
