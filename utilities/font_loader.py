from utilities.alphabet import Alphabet
from PIL import ImageFont
import random
import os

class Font:
    def __init__(self, file, alphabet):
        self.file = file
        self.alphabet = alphabet

    def get_pil_font(self, fontsize):
        # fontsize = int(fontsize * self.size) # no size adjustments, since it's
        # gonna get supersized anyway. Just supersize it a bit more if needed.
        resource_dir = os.path.dirname(os.path.realpath(__file__)) + "/../resources"
        return ImageFont.truetype(resource_dir + "/fonts/" + self.file, fontsize)

FONT_LIST = [

    # keep alphabetical order

    Font("Inconsolata-Bold.ttf", Alphabet.LatinExtended),
    Font("Inconsolata-Regular.ttf", Alphabet.LatinExtended),
    Font("merchant-copy.regular.ttf", Alphabet.Latin), # only latin
    Font("merchant-copy.wide.ttf", Alphabet.Latin), # only latin
    Font("OCRAEXT.TTF", Alphabet.Latin + Alphabet("ŠŽÝÁÍÉÚÓ" + "šžýáíéúó")), # somewhat extended
    Font("RobotoMono-Bold.ttf", Alphabet.LatinExtended),
    Font("RobotoMono-Regular.ttf", Alphabet.LatinExtended),
    Font("VT323-Regular.ttf", Alphabet.LatinExtended),
]

class FontLoader:
    def get_font(file):
        for f in FONT_LIST:
            if f.file == file:
                return f
        raise Exception("Font '%s' does not exist." % file)

    def get_random_font():
        return random.choice(FONT_LIST)
