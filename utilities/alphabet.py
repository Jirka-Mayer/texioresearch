import string

class Alphabet:
    def __init__(self, characters):
        """Private constructor"""
        self.characters = characters

    def encode(self, text):
        encoded = list(map(
            lambda x: self.characters.find(x),
            text
        ))
        if -1 in encoded:
            raise Exception("Given string '%s' cannot be encoded." % text)
        return encoded

    def decode(self, sequence):
        return "".join(list(map(
            lambda x: self.characters[x],
            sequence
        )))

    @property
    def size(self):
        return len(self.characters)

    def __add__(a, b):
        return Alphabet(a.characters + b.characters) # assume they don't overlap

########################
# Alphabet definitions #
########################

Alphabet.Space = Alphabet(" ")

Alphabet.LatinCharactersLowercase = Alphabet(string.ascii_lowercase)
Alphabet.LatinCharactersUppercase = Alphabet(string.ascii_uppercase)
Alphabet.LatinCharacters = Alphabet.LatinCharactersUppercase + Alphabet.LatinCharactersLowercase

Alphabet.LatinNumbers = Alphabet(string.digits)
Alphabet.LatinSymbols = Alphabet(".:,;(*!?')+-=_/%\"@&$#[]")

Alphabet.LatinNoSpace = Alphabet.LatinCharacters \
    + Alphabet.LatinNumbers \
    + Alphabet.LatinSymbols
Alphabet.Latin = Alphabet.LatinNoSpace + Alphabet.Space

Alphabet.LatinExtendedOnly = Alphabet("ĚŠČŘŽÝÁÍÉŮÚŇŤĎÓ" + "ěščřžýáíéůúňťďó")
Alphabet.LatinExtended = Alphabet.Latin + Alphabet.LatinExtendedOnly
