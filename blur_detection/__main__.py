import numpy as np
import cv2
import matplotlib.pyplot as plt
import shlex
import json

# best threshold seems to be 300
# 
# lowest sharp was around 500
# highest blurred was around 250

NORMALIZED_WIDTH = 800
DATASET = "blur_detection/blur_detection_dataset.json"


def mark_file(file_name):
    """Marks a file whether it's blurry or not"""
    img = cv2.imread(file_name, 0)

    # normalize image width (the smaller side)
    factor = NORMALIZED_WIDTH / min(*img.shape)
    img = cv2.resize(img, dsize=(int(img.shape[1]*factor), int(img.shape[0]*factor)))

    laplacian = int(cv2.Laplacian(img, cv2.CV_64F).var())

    plt.imshow(np.dstack([img, img, img]))
    plt.show(block=False)

    label = int(input("Blurriness (" + str(laplacian) + ") [0/1/2]: "))

    if label not in [0, 1, 2]:
        exit("Invalid value, exitting.")

    dataset = json.load(open(DATASET, "r"))
    dataset[file_name] = {
        "laplacian": laplacian,
        "label": label
    }
    json.dump(dataset, open(DATASET, "w"), indent=4)


def main():
    """Main method of the program"""
    while True:
        file_names = shlex.split(input("Paste files: "))

        if len(file_names) == 0:
            return

        for f in file_names:
            mark_file(f)


if __name__ == "__main__":
    main()
