import cv2
import numpy as np
import math
from bisect import bisect_left, bisect_right

class Circle:
    def __init__(self, center, radius, contour):
        self.c = center
        self.r = radius

        self.contour = contour

        self.component = None

        self.outlier = False

    def intersects_with(self, other):
        # shrink horizontal distance (3x)
        # grow vertical distance (120%)
        d_squared = ((self.c[0] - other.c[0]) / 3) ** 2 \
                    + ((self.c[1] - other.c[1]) * 1.2) ** 2
        r_squared = (self.r + other.r) ** 2
        return d_squared <= r_squared

    def draw(self, img, color=(0, 255, 0)):
        if self.outlier:
            color = (0, 0, 255)

        cv2.circle(img, (int(self.c[0]), int(self.c[1])), int(self.r), color, 1)

class Component:
    median_component_height = None

    def __init__(self):
        self.circles = []

        self.rect = None
        self.angle_rad = None

        self.big = False
        self.small = False

        self.skew_coef = None

        # word data
        self.image = None
        self.text = None

    def append(self, c):
        self.circles.append(c)

    def calculate_rect(self):
        points = np.empty(shape=(0, 2), dtype=np.float32)
        for c in self.circles:
            points = np.append(points, c.contour[:,0,:], axis=0)
        self.rect = list(cv2.minAreaRect(np.int0(points)))

        # repair orientation (deg)
        if self.rect[2] < -45:
            self.rect[2] += 90
            self.rect[1] = (self.rect[1][1], self.rect[1][0])
        if self.rect[2] > 45:
            self.rect[2] -= 90
            self.rect[1] = (self.rect[1][1], self.rect[1][0])

        self.angle_rad = self.rect[2] / 180 * math.pi

    def calculate_weirdness(self):
        self.big = self.rect[1][1] > Component.median_component_height * 2.0
        self.small = self.rect[1][1] < Component.median_component_height / 2.0

    def draw(self, img):
        color = (0, 0, 255)
        width = 2
        if self.big:
            color = (255, 0, 0)
        if self.small:
            color = (255, 200, 200)
            width = 1

        box = np.int0(cv2.boxPoints(tuple(self.rect)))
        cv2.drawContours(img, [box], 0, color, width)

    def on_same_line_as(self, that):
        dx = that.rect[0][0] - self.rect[0][0]
        dy = self.skew_coef * dx
        return abs(that.rect[0][1] - self.rect[0][1] - dy) <= Component.median_component_height;

    def __lt__(a, b):
        if a.on_same_line_as(b): # is vertical position roughly equal?
            return a.rect[0][0] < b.rect[0][0] # then order horizontally
        else:
            return a.rect[0][1] < b.rect[0][1] # else order vertically

    def cut_from(self, img):
        normalize_height = True
        padding = 4

        if normalize_height:
            h = 32 # normalize to 32px height
            w = int(h * (self.rect[1][0] / self.rect[1][1]))
        else:
            w = int(self.rect[1][0])
            h = int(self.rect[1][1])

        box = cv2.boxPoints(tuple(self.rect))
        M = cv2.getPerspectiveTransform(
            box,
            np.array([
                [padding, h - 2 * padding],
                [padding, padding],
                [w - 2 * padding, padding],
                [w - 2 * padding, h - 2 * padding]
            ], dtype=np.float32)
        )
        return cv2.warpPerspective(img, M, (w, h))

class Line:
    def __init__(self):
        self.words = []

    def append(self, word):
        self.words.append(word)

    def draw(self, img):
        points = np.empty(shape=(0, 2), dtype=np.float32)
        for w in self.words:
            for c in w.circles:
                points = np.append(points, c.contour[:,0,:], axis=0)
        rect = cv2.minAreaRect(np.int0(points))

        box = np.int0(cv2.boxPoints(rect))
        cv2.drawContours(img, [box], 0, (0, 0, 255), 2)

def find_words(img, tee_store=None):

    # normalize image width
    width = 930
    height = int(width * (img.shape[0] / img.shape[1]))
    img = cv2.resize(img, dsize=(width, height))

    # get contours
    edges = cv2.Canny(img, 30, 50, apertureSize=3)
    _, contours, _ = cv2.findContours(
        edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
    )

    ###############
    # Get circles #
    ###############
    #
    # Use bins to speed up neighbouring circle search in DFS
    #   Turns O(n^2) into O(n) time. (44s -> 11s for 32 receipts)
    # 

    circles = []
    bin_size = 100
    bins_x = math.ceil(width / bin_size)
    bins_y = math.ceil(height / bin_size)
    #circle_bins = [[[[]] * bins_y] * bins_x]
    circle_bins = [( [ [] for j in range(bins_y)] ) for i in range(bins_x)]
    for c in contours:
        circ = cv2.minEnclosingCircle(c)
        rect = cv2.minAreaRect(c)

        # remove circles touching an edge of the image (probbably some background noise)
        if circ[0][0] - circ[1] <= 0: continue
        if circ[0][0] + circ[1] >= width: continue
        if circ[0][1] - circ[1] <= 0: continue
        if circ[0][1] + circ[1] >= height: continue
        
        # remove line segments (narrow rectangles, large circles)
        circ_area = circ[1] ** 2 * math.pi
        rect_area = rect[1][0] * rect[1][1]
        if circ_area > rect_area * 5.0: # magic constant
            continue
        
        # remove tiny noise
        if circ[1] < 5:
            continue
        
        # remove huge mess
        if circ[1] > 50: # 40 was ok (almost everywhere)
            continue

        circle = Circle(circ[0], circ[1], c)
        circles.append(circle)
        
        x = math.floor(circle.c[0] / bin_size)
        y = math.floor(circle.c[1] / bin_size)
        circle_bins[x][y].append(circle)

    #################################
    # Group circles into components #
    #################################

    # depth first search
    def dfs(circle, component):
        component.append(circle)
        circle.component = component
        
        # for all nearby circles
        x = math.floor(circle.c[0] / bin_size)
        y = math.floor(circle.c[1] / bin_size)
        for bx in range(max(0, x - 1), min(bins_x, x + 2)):
            for by in range(max(0, y - 1), min(bins_y, y + 2)):
                for c in circle_bins[bx][by]:
                    if c.component is not None: continue # skip closed
                    if c.outlier: continue # skip outliers
                    if circle.intersects_with(c): # iterate over neighbors
                        dfs(c, component)

    # group circles into words
    components = []
    for c in circles:
        if c.component is None: # skip closed verts
            current_component = Component()
            dfs(c, current_component)
            components.append(current_component)

    # process components
    for c in components:
        c.calculate_rect()

    # remove too thin components (5px or less)
    components = [c for c in components if c.rect[1][1] > 5]

    #####################################################
    # Process weird components - remove outlier circles #
    #####################################################

    # get median component height
    Component.median_component_height = np.median(np.array(
        list(map(lambda x: x.rect[1][1], components))
    ))
    
    # find weird components (too big or too small)
    for c in components:
        c.calculate_weirdness()

    # separate big components and remove small components
    big_components = [c for c in components if c.big]
    components = [c for c in components if not c.big and not c.small]
    
    # find outliers of big components
    for component in big_components:
        norm = [-math.sin(component.angle_rad), math.cos(component.angle_rad)] # normal vector to which to project
        positions = [(c.c[0] - component.rect[0][0]) * norm[0] + (c.c[1] - component.rect[0][1]) * norm[1] for c in component.circles]
        radii = [c.r for c in component.circles]

        # find outliers
        distance_threshold = 10.0 # magic
        neighbor_threshold = 5 # magic
        #outliers = [] # for plotlib
        for i in range(len(positions)):
            neighbors = 0
            for j in range(len(positions)):
                if i == j: continue
                if abs(positions[i] - positions[j]) + abs(radii[i] - radii[j]) < distance_threshold: # manhattan
                    neighbors += 1
            if neighbors < neighbor_threshold:
                #outliers.append(i)
                component.circles[i].outlier = True

        """
        # plotting

        out_radii = [radii[i] for i in outliers]
        out_positions = [positions[i] for i in outliers]

        import matplotlib.pyplot as plt
        plt.plot(radii, positions, "bo")
        plt.plot(out_radii, out_positions, "rx")
        plt.show()
        """

        # free up component circles and rebuild new components with DFS
        for c in component.circles:
            c.component = None
        
        for c in component.circles:
            if c.component is None and not c.outlier: # skip closed verts
                current_component = Component()
                dfs(c, current_component) # DFS ignores outliers
                components.append(current_component)

                # process the component
                current_component.calculate_rect()

    # tee render result
    if tee_store is not None:
        tee_img = np.dstack([img, img, img])
        for c in circles:
            c.draw(tee_img)
        for c in components:
            c.draw(tee_img)
        tee_store.store_step("components", tee_img)

    #################
    # Line building #
    #################

    # sort components roughly by vertical position first
    components.sort(key=lambda x: x.rect[0][1])

    # calculate local skew coefficients ("angles" of the lines)
    window_semi_height_px = Component.median_component_height * 5
    y_positions = list(map(lambda x: x.rect[0][1], components))
    weights = list(map(lambda x: x.rect[1][0], components))
    weights_sum = np.cumsum(weights)
    weighted_coeffs = list(map(lambda x: math.tan(x.angle_rad) * x.rect[1][0], components))
    weighted_coeffs_sum = np.cumsum(weighted_coeffs)
    for i in range(len(components)):
        start_i = bisect_left(y_positions, y_positions[i] - window_semi_height_px, hi=i)
        end_i = bisect_right(y_positions, y_positions[i] + window_semi_height_px, lo=i, hi=len(components) - 1)
        if start_i != end_i:
            coef = (weighted_coeffs_sum[end_i] - weighted_coeffs_sum[start_i]) / (weights_sum[end_i] - weights_sum[start_i])
        else:
            coef = weighted_coeffs[end_i] / weights[end_i]
        components[i].skew_coef = coef

    # now sort components properly with horizontal consideration
    components.sort()

    # find lines
    lines = []
    current_line = Line()
    last_word = None
    for word in components:
        if last_word is None:
            current_line.append(word)
            last_word = word
            continue

        if word.on_same_line_as(last_word):
            current_line.append(word)
            last_word = word
        else:
            lines.append(current_line)
            current_line = Line()
            current_line.append(word)
            last_word = word
    lines.append(current_line)

    # tee render result
    if tee_store is not None:
        tee_img = np.dstack([img, img, img])
        for l in lines:
            l.draw(tee_img)
        tee_store.store_step("lines", tee_img)

    ################
    # Word cutting #
    ################

    # cut words out
    for c in components:
        c.image = c.cut_from(img)

    return lines, components # components = words now
