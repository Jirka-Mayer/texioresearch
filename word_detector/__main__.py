import re
import cv2
import numpy as np
import argparse
import os
import pickle
from word_detector.detector import find_words

def main(file=None):
    if file is None:
        file = "resources/receipts/restaurace-osma_2018-08-23.png"
    img = cv2.imread(file, 0)
    lines, words = find_words(img)

    from line_ctc_classifier.network import Network
    network = Network(threads=1)
    network.construct(logdir=None)
    network.load("K_postprocessing_phase")

    print("===============================")
    
    i = 0
    for w in words:
        w.text = network.predict(w.image)
        i += 1
        
        print("%d/%d" % (i, len(words)))

    print("===============================")

    for l in lines:
        print("\t".join([w.text for w in l.words]))

def find_words_main():
    receipts = os.listdir("resources/receipts")
    
    class Tee:
        def __init__(self, r):
            self.r = r

        def store_step(self, name, img):
            cv2.imwrite("logs/" + name + "_" + self.r + ".png", img)

    # sort alphabetically
    receipts.sort()

    for receipt in receipts:
        img = cv2.imread("resources/receipts/" + receipt, 0)
        
        find_words(img, Tee(receipt))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("action", type=str)
    parser.add_argument("param", type=str)
    args = parser.parse_args()

    if args.action == "main":
        main(args.param)
    elif args.action == "words":
        find_words_main()
    else:
        print("Unknown action.")
