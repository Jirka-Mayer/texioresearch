# Researching stuff

Lanuching various scripts:

    python3 -m image_generator
    python3 -m line_ctc_classifier

Receipt font suggestions:

https://graphicdesign.stackexchange.com/questions/20027/what-font-is-typically-used-for-receipts

HTR example:

https://github.com/githubharald/SimpleHTR


# Known issues

- word detector throws away circles touching the edge of the image. It should properly throw away circles touching the outer receipt contour. That way it will be less prone to making a mistake. Thus the transformed contour need to be stored and passed further down the pipeline.
- word detector may remove logos/bar codes/QR codes before word detection begins
    - hint: try using dilation and erosion to remove text and glue barcodes
